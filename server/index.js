const express = require('express'),
    app = express(),
    mongoose = require('mongoose'),
    jokesRoute = require('./routes/routes.jokes'),
    bodyParser = require('body-parser'),
    cors = require('cors');
// =================== initial settings ===================
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

// connnect to mongo-connects Mongoose to the database:
mongoose.connect('mongodb://127.0.0.1/jokes', () => {
    console.log('connected to mongodb');
})
// routes
app.use('/jokes', jokesRoute);

// Set the server to listen on port 3000
const port = process.env.PORT || 3000
//process.env.PORT will be used when we deploy...
app.listen(port,( )=>{
    console.log('***server running on port***', port)
})

