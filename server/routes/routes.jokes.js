// Creating our jokes routes folder this will be our API to add, remove, find and find jokes.

const express     = require('express'), 
    router        = express.Router(),
    controller    = require('../controllers/controllers.jokes.js');

//  == This route will give us back all jokes: ==  //
router.get('/', controller.findAll);

//  == This route allow us to add an extr joke: ==  //
router.post('/new', controller.insert);

//  == This route allow us to delete one joke, it will be that with the id we are providing: ==  //
router.post('/delete', controller.delete);



    module.exports = router;