// Now that we have our model and routes we need to create our controller, which will handle the logic to perfrom the actions described in the routes.
// So in the controllers folder we should create JokesController.
// Because it will contain all methods of the jokes schema and routes we can have it in one class.
const Jokes = require('../models/models.jokes.js');


class JokesController {
    // GET FIND ALL
    async findAll(req, res){
        try{
            const jokes = await Jokes.find({});
            res.send(jokes);
        }
        catch(e){
            res.send({e})
        }
    }
    
    // POST ADD ONE
    async insert (req, res) {
        let { joke } = req.body;
        try{
            const done = await Jokes.create({ joke });
            res.send(done)
        }
        catch(e){
            res.send({e})
        }
    }

    // DELETE 
    async delete (req, res) {
        let { _id } = req.body;
        try{
            const joke = await Jokes.remove({_id});
            res.send(joke);   
        }
        catch(e){
            res.send({e})
        }
    }
}

module.exports = new JokesController();
