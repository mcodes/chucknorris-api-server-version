// Now in the model folder we can create a schema which will define the structure of a mongo collection.
// Schema is the way in which Mongoose controls what kind of data should be added to the DB. A single DB 
//instance contains several Collections of objects. For each Collection we need to create a new schema.
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const jokesSchema = new Schema({
    joke: String,
    id: Number,
})
module.exports =  mongoose.model('jokes', jokesSchema);
