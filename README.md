## Instructions

Creating a full stack application that makes a get request to the Chuck Norris API (http:*//api.icndb.com/jokes/random*) and based on the joke you can add it to your favorites, as well as the ability to remove it.

Full stack app version using backend server with express. 

You can see a working demo of the app (localstorage version) in the following link: http://http://frightened-part.surge.sh/